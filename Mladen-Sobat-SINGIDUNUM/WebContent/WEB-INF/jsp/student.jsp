<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Students</title>
</head>
<body>
	<table style="">
    <tr>
        <th>ID</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Index</th>
        <th>City</th>
        <th>Address</th>
        <th>CurrentYearOfStudy</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Finance</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="student" items="${students}">
	    <tr>
	        <td>
	            <c:out value="${student.id}" />
	        </td>
	        <td>
	            <c:out value="${student.firstName}" />
	        </td>
	        <td>
	            <c:out value="${student.lastName}" />
	        </td>
	        <td>
	            <c:out value="${student.indexNumber}" />
	        </td>
	        <td>
	            <c:out value="${student.city.cityName}" />
	        </td>
	        <td>
	            <c:out value="${student.adress}" />
	        </td>
	        <td>
	            <c:out value="${student.currentYearOfStudy}" />
	        </td>
	        <td>
	            <c:out value="${student.email}" />
	        </td>
	        <td>
	            <c:out value="${student.phone}" />
	        </td>
	        <td>
	        	<a href="${pageContext.request.contextPath}/students/edit/${student.id}">Edit student</a>
	        </td>
	        <td>
	        	<a href="${pageContext.request.contextPath}/students/delete/${student.id}">Delete student</a>
	        </td>
	    </tr>
    </c:forEach>
   	</table>
   	<a href="${pageContext.request.contextPath}/">Home</a>
    <a href="${pageContext.request.contextPath}/student/create">Create student</a>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/subjects/add" method="post" id="subjectform">
		<label>Subject name: </label><input type="text" name="name" required/> <br/>
		<label>Description: </label><textarea name="description" form="subjectform" maxlength=200></textarea> <br/>
		<label>Year of study: </label>
			<select name="yearOfStudy">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
			</select>
		<br/>
		<label>Semester: </label>
			<select name="semester">
				<option value="FIRST">FIRST</option>
				<option value="SECOND">SECOND</option>
				<option value="THIRD">THIRD</option>
				<option value="FOURTH">FOURTH</option>
				<option value="Fifth">Fifth</option>
				<option value="Sixth">Sixth</option>
				<option value="Seventh">Seventh</option>
				<option value="Eighth">Eighth</option>
			</select>
		
		<label>Select professor: </label> 
			<select name="id">
				<c:forEach var="professor" items="${profesor}">
					<option value="${profesor.id}">${profesor.firstName} ${profesor.lastName}</option>
				</c:forEach>
			</select>
		<br/>
		
		<input type="submit" value="Add subject"/>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    <c:forEach var="titleObject" items="${titles}">
	    <tr>
	        <td>
	            <c:out value="${titleObject.titleId}" />
	        </td>
	        <td>
	            <c:out value="${titleObject.titleName}" />
	        </td>
	    </tr>
    </c:forEach>
   	</table>
   	<a href="${pageContext.request.contextPath}/">Home</a>
    <a href="${pageContext.request.contextPath}/title/create">Create title</a>
	
</body>
</html>
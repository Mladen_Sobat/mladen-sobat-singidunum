<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table style="">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Year of study</th>
        <th>Semester</th>
        <th>Subjects</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="subject" items="${subjects}">
	    <tr>
	        <td>
	            <c:out value="${subject.subjectId}" />
	        </td>
	        <td>
	            <c:out value="${subject.yearOfStudy}" />
	        </td>
	        <td>
		        <textarea rows="4" cols="50">
					<c:out value="${subject.description}" />
				</textarea>
	        </td>
	        <td>
	        	<textarea rows="4" cols="50">
	        		<c:forEach var="professor" items="${subject.professors}">
		            	<c:out value="${subject.description}" />
		            </c:forEach>
				</textarea>
	        </td>
	        <td>
	        	<a href="${pageContext.request.contextPath}/subjects/edit/${subject.subjectId}">Edit subject</a>
	        </td>
	        <td>
	        	<a href="${pageContext.request.contextPath}/subjects/delete/${subject.subjectId}">Delete subject</a>
	        </td>
	    </tr>
    </c:forEach>
   	</table>
   	<a href="${pageContext.request.contextPath}/">Home</a>
    <a href="${pageContext.request.contextPath}/subject/create">Create subject</a>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Crete NEW Profesor</title>

</head>
<body>
<form action="${pageContext.request.contextPath}/doCreateProfesor" method="post">
      	<label>FirstName: </label><input type="text" name="firstName" required/> <br/>
		<label>LastName: </label><input type="text" name="lastName" required/> <br/>
		<label>Email: </label><input type="email" name="email"/> <br/>
		<label>Phone: </label><input type="text" name="phone"/> <br/>
		<label>Address: </label><input type="text" name="adress"/> <br/>
		<label>Title: </label> 
			<select name="title_id">
				<option value="">Select option</option>
				<c:forEach var="title" items="${titles}">
					<option value="${title.title_id}">${title.titleName}</option>
				</c:forEach>
			</select>
		<br/>
		<label>City: </label>
			<select name="id">
				<c:forEach var="city" items="${cities}">
					<option value="${city.id}">${city.name}</option>
				</c:forEach>
			</select>
		 <br/>
 		 <label>Re-election date: </label><input type="date" id="reelectionDate" name="date" required />
 		 <br/>
		
		<input type="submit" value="Add professor"/>
	</form>
	<script type="text/javascript">
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		 if(dd<10){
		        dd='0'+dd
		    } 
		    if(mm<10){
		        mm='0'+mm
		    } 
	
		today = yyyy+'-'+mm+'-'+dd;
		document.getElementById("Date").setAttribute("min", today);
	</script>



</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<style type=text/css>
 .table a
{
    display:block;
    text-decoration:none;
}
</style>
</head>
<body>
<table width="400" border="1" class="table">
    <tr>
        <td><a href="${pageContext.request.contextPath}/profesor">Show Professors</a></td>
        <td><a href="${pageContext.request.contextPath}/student">Show Students</a></td>
       
        <td><a href ="${pageContext.request.contextPath}/title">Show all title</a></td>
        <td><a href = "${pageContext.request.contextPath}/city">Show all cities</a><td>
        <td><a href = "${pageContext.request.contextPath}/subject">Show all subject</a><td>
    </tr>
    
    <tr>
        <td><a href="${pageContext.request.contextPath}/createNewStudent">Create NEW Student</a></td>
        <td><a href="${pageContext.request.contextPath}/createNewProfesor">Create NEW Profesor</a></td>
        <td><a href="${pageContext.request.contextPath}/createNewSubject">Create NEW Subject</a></td>
      <td><a href="${pageContext.request.contextPath}/createNewTitle">Create NEW Title</a></td>
      <td><a href = "${pageContext.request.contextPath}/cityNewCity">Create NEW  City</a><td>
  </tr>
    
</table>
</body>
</html>
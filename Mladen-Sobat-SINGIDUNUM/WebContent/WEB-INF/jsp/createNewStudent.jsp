<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create New Student</title>


</head>
<body>
      	
	<form action="${pageContext.request.contextPath}/doCreateStudent" method="post">
		<label>FirstName: </label><input type="text" name="name"/> <br/>
		<label>LastName: </label><input type="text" name="lastName"/> <br/>
		<label>Email: </label><input type="email" name="email"/> <br/>
		<label>Phone: </label><input type="text" name="phone"/> <br/>
		<label>Address: </label><input type="text" name="adress"/> <br/>
		<label>CurrentYearOfStudy: </label>
			<select name="currentYearOfStudy">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
			</select>
		<br/>
		<label>Index: </label><input type="text" name="indexNumber"/> <br/>
		<label>City: </label> 
			<select name="id">
				<c:forEach var="city" items="${cities}">
					<option value="${city.id}">${city.name}</option>
				</c:forEach>
			</select>
		 <br/>
		
		<input type="submit" value="Add student"/>
	</form>
	


</form>
        

    
     
     
</body>
</html>
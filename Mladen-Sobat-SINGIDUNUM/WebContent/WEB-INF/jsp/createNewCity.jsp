<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New city</title>
</head>
<body>
	
	<form action="${pageContext.request.contextPath}/cities/add" method="post">
		<label>City name: </label><input type="text" name="cityName"/> <br/>
		
		<input type="submit" value="Add city"/>
</body>
</html>
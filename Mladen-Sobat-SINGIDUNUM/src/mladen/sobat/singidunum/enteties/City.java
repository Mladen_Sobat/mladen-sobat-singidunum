package mladen.sobat.singidunum.enteties;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "city")
public class City {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "city_id")
	private int id;
	

	@Size(min =2,max =30,message ="Name of the city should be 2 and 30 characters!")
	@Column(unique = true,name ="name")
	private String name;
	
	
	@Size(min =2,max =30,message ="Name of the country should be 2 and 30 characters!")
	@Column(name ="country")
	private String country;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "city_id")
	private Set<Student> students;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "city_id")
	private Set<Profesor> professors;
	
	public City() {
		
	}

	public City(int id,
			@NotNull @Size(min = 2, max = 30, message = "Name of the city should be 2 and 30 characters!") String name,
			@NotNull @Size(min = 2, max = 30, message = "Name of the country should be 2 and 30 characters!") String country,
			Set<Student> students, Set<Profesor> professors) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
		this.students = students;
		this.professors = professors;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Profesor> getProfessors() {
		return professors;
	}

	public void setProfessors(Set<Profesor> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + ", country=" + country + ", students=" + students + ", professors="
				+ professors + "]";
	}
	
}

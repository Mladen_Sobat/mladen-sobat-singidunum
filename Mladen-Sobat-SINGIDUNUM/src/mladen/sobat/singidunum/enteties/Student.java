package mladen.sobat.singidunum.enteties;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "student")
public class Student {
	
	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Size(min = 2,max = 30, message = "Name should be between 2 and 30 characters!")
	@Column(name = "name")
	private String name;
	@NotNull
	@Size(min =3,max = 30,message = "Last name should be between 3 and 30 characters!")
	@Column(name ="lastName")
	private String lastName;
	@NotNull
	@Email(message="This is not valid email")
	@Size(max =30,message ="Email should have 30 characters!")
	@Column(unique =true)
	private String email;
	@Size(min = 3, max = 50,message= "Adress should be between 3 and 40 characters!"  )
	private String adress;
	@ManyToOne
	@JoinColumn(name = "city")
	private City city;
	
	@Size(min = 5 , max = 15,message = "Phone should be between 5 and 15 character!")
	private String phone;
	@NotNull
	@Column(name="currentYearOfStudy")
	private int currentYearOfStudy;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "students_subjects", joinColumns = @JoinColumn(name = "student_id"), inverseJoinColumns = @JoinColumn(name = "subject_id"))
	private Set<Subject> subjects;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "exam_registration", joinColumns = @JoinColumn(name = "student_id"), inverseJoinColumns = @JoinColumn(name = "exam_id"))
	private Set<Exsams> exams;

	public Student() {

	}

	public Student(int id, String name, String lastName, String email, String adress, City city, String phone,
			int currentYearOfStudy) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}
	

	public Set<Exsams> getExams() {
		return exams;
	}

	public void setExams(Set<Exsams> exams) {
		this.exams = exams;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", lastName=" + lastName + ", email=" + email + ", adress="
				+ adress + ", city=" + city + ", phone=" + phone + ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}

	
	
}

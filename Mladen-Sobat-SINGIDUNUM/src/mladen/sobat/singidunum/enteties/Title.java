package mladen.sobat.singidunum.enteties;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name ="titles")
public class Title {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name ="title_id")
	private int title_id;
	@NotNull
	@Size(min =3 ,max = 30,message =" Title should be between 3 and 30 characters!")
	@Column(unique =true,name ="title_name")
	private String titleName;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "title_id")
	private Set<Profesor> profesors;
	
	public Title() {
		
	}
	public Title(int title_id, String titleName) {
		super();
		this.title_id = title_id;
		this.titleName = titleName;
	}
	public int getTitle_id() {
		return title_id;
	}
	public void setTitle_id(int title_id) {
		this.title_id = title_id;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public Set<Profesor> getProfesors() {
		return profesors;
	}
	public void setProfesors(Set<Profesor> profesors) {
		this.profesors = profesors;
	}
	@Override
	public String toString() {
		return "Title [title_id=" + title_id + ", titleName=" + titleName + ", profesors=" + profesors + "]";
	}
	
}
	
	
	


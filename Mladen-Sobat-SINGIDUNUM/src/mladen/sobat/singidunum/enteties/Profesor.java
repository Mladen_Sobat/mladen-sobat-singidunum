package mladen.sobat.singidunum.enteties;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Entity
@Table(name = "profesor")
public class Profesor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "profesor_id")
	private int profesor_id;
	@NotNull
	@Size(min =3,max=30,message ="First name sholud be between 3 and 30 characters!")
	@Column(name ="firstName")
	private String firstName;
	
	@NotNull
	@Size(min = 3,max = 30,message ="Last name should be between 3 and 30 characters!")
	@Column(name ="lastName")
	private String lastName;
	
	@Email(message ="This is not valid email")
	@Size(max = 30,message ="Email should have 30 characters" )
	@Column(unique = true)
	private String email;
	
	@Size(min =3 ,max=40,message = "Adress should be between 3 and 40 characters!")
	private String adress;
	
	@ManyToOne
	@JoinColumn(name = "city")
	private City city;
	
	@Size(min =5 ,max =15,message ="Phone should be between 5 and 15 characters!")
	private String phone;
	
	@NotNull
	@Column(name ="date")
	private Date date;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "title_id")
	private Title title;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "profesor_id")
	private Set<Exsams> exams;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "professors_subjects", joinColumns = @JoinColumn(name = "professor_id"), inverseJoinColumns = @JoinColumn(name = "id_subject"))
	private Set<Subject> subjects;
	
	public Profesor() {
		
	}
	public Profesor(int profesor_id, String firstName, String lastName, String email, String adress, City city,
			String phone, Date date, Title title) {
		super();
		this.profesor_id = profesor_id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.date = date;
		this.title = title;
	}
	public int getProfesor_id() {
		return profesor_id;
	}
	public void setProfesor_id(int profesor_id) {
		this.profesor_id = profesor_id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Title getTitle() {
		return title;
	}
	public void setTitle(Title title) {
		this.title = title;
	}
	
	public Set<Exsams> getExams() {
		return exams;
	}
	public void setExams(Set<Exsams> exams) {
		this.exams = exams;
	}
	public Set<Subject> getSubjects() {
		return subjects;
	}
	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}
	@Override
	public String toString() {
		return "Profesor [profesor_id=" + profesor_id + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", adress=" + adress + ", city=" + city + ", phone=" + phone + ", date=" + date
				+ ", title=" + title + ", exams=" + exams + ", subjects=" + subjects + "]";
	}

	
	
	
	
}

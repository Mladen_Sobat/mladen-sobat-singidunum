package mladen.sobat.singidunum.enteties;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@Entity
@Table(name ="exsams")
public class Exsams {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exsams_id")
	private int id;
	
	@NotNull
	@Column(name = "date")
	private Date date;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_subject")
	private Subject subject;
	
	@ManyToMany(mappedBy = "exsams")
	private Set<Student> students;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name ="profesor_id")
	private Profesor profesor;
	
	public Exsams() {
		
	}

	public Exsams(int id, @NotNull Date date, @NotNull Subject subject, Set<Student> student,
			@NotNull Profesor profesor) {
		super();
		this.id = id;
		this.date = date;
		this.subject = subject;
		this.students = students;
		this.profesor = profesor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Set<Student> getStudent() {
		return students;
	}

	public void setStudent(Student student) {
		this.students = students;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	@Override
	public String toString() {
		return "Exsams [id=" + id + ", date=" + date + ", subject=" + subject + ", student=" + students + ", profesor="
				+ profesor + "]";
	}

	
		
	}
	
	
	


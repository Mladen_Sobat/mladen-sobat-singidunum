package mladen.sobat.singidunum.enteties;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name ="subject")
public class Subject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id_subject")
	private int id_subject;
	
	@Size(min =3,max = 150,message ="Description must be at least 3 and 150 characters! ")
	private String description;
	@Max(1)
	@Column(name ="yearOfStudy")
	private int yearOfStudy;
	@Enumerated(EnumType.STRING)
	private String samester;
	
	@ManyToMany(mappedBy = "subject")
	private Set<Profesor> proffesor;
	
	@ManyToMany(mappedBy = "subject")
	private Set<Student> students;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "subject_id")
	private Set<Exsams> exams;
	
	public enum Semester{
		FIRST,SECOND,THIRD,FOURTH,Fifth,Sixth,Seventh,Eighth
	}
	public Subject() {
		
	}
	public Subject(int id_subject, String description, int yearOfStudy, String samester) {
		super();
		this.id_subject = id_subject;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.samester = samester;
	}
	public int getId_subject() {
		return id_subject;
	}
	public void setId_subject(int id_subject) {
		this.id_subject = id_subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getYearOfStudy() {
		return yearOfStudy;
	}
	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}
	public String getSamester() {
		return samester;
	}
	public void setSamester(String samester) {
		this.samester = samester;
	}
	
	public Set<Profesor> getProffesor() {
		return proffesor;
	}
	public void setProffesor(Set<Profesor> proffesor) {
		this.proffesor = proffesor;
	}
	public Set<Student> getStudents() {
		return students;
	}
	public void setStudents(Set<Student> students) {
		this.students = students;
	}
	public Set<Exsams> getExams() {
		return exams;
	}
	public void setExams(Set<Exsams> exams) {
		this.exams = exams;
	}
	@Override
	public String toString() {
		return "Subject [id_subject=" + id_subject + ", description=" + description + ", yearOfStudy=" + yearOfStudy
				+ ", samester=" + samester + "]";
	}
	
}

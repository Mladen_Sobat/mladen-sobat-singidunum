package mladen.sobat.singidunum.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import mladen.sobat.singidunum.enteties.Subject;
@Component("subjectDao")
public class SubjectDAO {
	 private NamedParameterJdbcTemplate jdbc;
	 
	 public void setDataSource(DataSource ds) {
		 this.jdbc = new NamedParameterJdbcTemplate(ds);
		 
	 }
	 public List<Subject>getSubjects(){
		 return jdbc.query("SELECT *FROM subject ", new RowMapper<Subject>() {

			@Override
			public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
				Subject subject = new Subject();
				
				subject.setId_subject(rs.getInt("id"));
				subject.setDescription(rs.getString("description"));
				subject.setSamester(rs.getString("semester"));
				subject.setYearOfStudy(rs.getInt("yearOfStudy"));
				
				return subject;
			}
		});
	 }
	 public Subject getSubjectById(int id) {
		 MapSqlParameterSource params =new MapSqlParameterSource();
		 params.addValue("id", id);
		 return jdbc.queryForObject("SELECT * FROM subject where id=:id",params , new RowMapper<Subject>() {

			@Override
			public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
				Subject subject = new Subject();
				
				subject.setId_subject(rs.getInt("id"));
				subject.setDescription(rs.getString("description"));
				subject.setSamester(rs.getString("semester"));
				subject.setYearOfStudy(rs.getInt("yearOfStudy"));
				
				return subject;
			}
			 
		 });
	 }
	 public boolean delete (int id) {
		 MapSqlParameterSource params = new MapSqlParameterSource();
		 params.addValue("id", id);
		 return jdbc.update("DELETE FROM subject where id=:id", params)==1;
		 
	 }
	 public boolean create (Subject subject ) {
		 MapSqlParameterSource params  = new MapSqlParameterSource();
		 params.addValue("id",subject.getId_subject());
		 params.addValue("description",subject.getDescription());
		 params.addValue("semester", subject.getSamester());
		 params.addValue("yearOfStudy", subject.getYearOfStudy());
		 
		 return jdbc.update("INSERT INTO subject (id,description,semester,yearOfStudy)"
				 + "VALUES (:id, :description,:semester,:yearOfStudy)"	,params)==1;	 
		 
		 
		 
		
		 
	 }
	 public boolean update(Subject subject ) {
		 BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(subject);
		 return jdbc.update("UPDATE subject SET description= :description,semester=:semester,yearOfStudy=:yearOfStudy WHERE id=;id", params)==1;
	 }
}

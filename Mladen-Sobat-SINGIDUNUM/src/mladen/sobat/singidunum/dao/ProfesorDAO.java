package mladen.sobat.singidunum.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mladen.sobat.singidunum.enteties.City;
import mladen.sobat.singidunum.enteties.Profesor;
import mladen.sobat.singidunum.enteties.Title;
import org.hibernate.SessionFactory;



@Component("profesorDao")
public class ProfesorDAO {
	
	
	private NamedParameterJdbcTemplate jdbc;
	@Autowired
	public void setDataSource(DataSource ds) {
		this.jdbc = new NamedParameterJdbcTemplate(ds);
		
		
	}
	public List<Profesor> getProfesori(){
		return jdbc.query("SELECT * FROM profesor ", new RowMapper<Profesor>() {

			@Override
			public Profesor mapRow(ResultSet rs, int rowNum) throws SQLException {
				Profesor profesor = new Profesor();
				
				profesor.setProfesor_id(rs.getInt("id"));
				profesor.setFirstName(rs.getString("name"));
				profesor.setLastName(rs.getString("lastname"));
				profesor.setEmail(rs.getString("Email"));
				profesor.setAdress(rs.getString("adresa"));
				profesor.setCity((City)rs.getObject("city"));
				profesor.setPhone(rs.getString("phone"));
				profesor.setDate(rs.getDate("date"));
				profesor.setTitle((Title)rs.getObject("title"));
				
				
				return profesor;
			}
			
		});
	}
	public Profesor getProfesorById(int id) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.queryForObject("SELECT * FROM profesor where id=:id",params, new RowMapper<Profesor>() {

			@Override
			public Profesor mapRow(ResultSet rs, int rowNum) throws SQLException {
			Profesor profesor =new Profesor();
			
			profesor.setProfesor_id(rs.getInt("id"));
			profesor.setFirstName(rs.getString("name"));
			profesor.setLastName(rs.getString("lastname"));
			profesor.setEmail(rs.getString("Email"));
			profesor.setAdress(rs.getString("adresa"));
			profesor.setCity((City)rs.getObject("city"));
			profesor.setPhone(rs.getString("phone"));
			profesor.setDate(rs.getDate("date"));
			profesor.setTitle((Title)rs.getObject("title"));
				return profesor;
			}
		});
	}
	public boolean delete (int id) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("DELETE from profesor where id=:id",params)==1;
	}
	public boolean create (Profesor profesor) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id",profesor.getProfesor_id());
		params.addValue("name",profesor.getFirstName());
		params.addValue("lastname", profesor.getLastName());
		params.addValue("Email",profesor.getEmail());
		params.addValue("adresa",profesor.getAdress());
		params.addValue("city", profesor.getCity());
		params.addValue("phone", profesor.getPhone());
		params.addValue("date",profesor.getDate());
		params.addValue("title", profesor.getTitle());
		
		return jdbc.update("INSERT INTO profesor (profesor_id,firstName,lastName,email,adress,city,phone,date,title )"
				+ "VALUES (:id,:name ,:lastname,:Email,:adresa,:city,:phone ,:date,:title)",params)==1;
	}
	
	public boolean update(Profesor profesor) {
		BeanPropertySqlParameterSource params =new BeanPropertySqlParameterSource(profesor);
		return jdbc.update("UPDATE profesor SET name=:name,lastname=:lastname ,Email=:email,adresa=:adresa,city=:city,phone=:phone,date=:date ,title=:title WHERE id=:id",params )==1;
	}

	}
	


	
	
	
	
	
	
	


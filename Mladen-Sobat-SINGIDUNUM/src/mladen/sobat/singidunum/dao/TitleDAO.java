package mladen.sobat.singidunum.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import mladen.sobat.singidunum.enteties.Title;

@Component("titleDao")
public class TitleDAO {
	private NamedParameterJdbcTemplate jdbc;
	@Autowired
	public void setDataSource(DataSource ds) {
		this.jdbc = new NamedParameterJdbcTemplate(ds);
		
	}
	public List<Title> getTitles(){
		return jdbc.query("SELECT * FROM titles",new RowMapper<Title>() {

			@Override
			public Title mapRow(ResultSet rs, int rowNum) throws SQLException {
			Title title = new Title();
			title.setTitle_id(rs.getInt("title_id"));
			title.setTitleName(rs.getString("title_name"));
				return title;
			}
		});
	}
	public Title getTitleById (int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.queryForObject("SELECT * FROM titles where title_id=:id", params,new RowMapper<Title>() {

			@Override
			public Title mapRow(ResultSet rs, int rowNum) throws SQLException {
				Title title = new Title();
				title.setTitle_id(rs.getInt("title_id"));
				title.setTitleName(rs.getString("title_name"));
				
				return title;
			}
		});
	}
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id",id);
		return jdbc.update("DELETE FROM titles where title_id=:id", params)==1;
	}
	public boolean create (Title title) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", title.getTitle_id());
		params.addValue("name",title.getTitleName());
		return jdbc.update("INSERT INTO titles (title_id,title_name)" + "VALUES ( :id,:name)",params)==1;
	}
	public boolean update(Title title) {
		BeanPropertySqlParameterSource  params= new BeanPropertySqlParameterSource(title);
		return jdbc.update("UPDATE titles SET title_name=:name where title_id=:id",params)==1;
	}
}

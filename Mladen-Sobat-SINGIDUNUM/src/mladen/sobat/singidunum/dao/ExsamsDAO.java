package mladen.sobat.singidunum.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import mladen.sobat.singidunum.enteties.Exsams;
import mladen.sobat.singidunum.enteties.Profesor;
import mladen.sobat.singidunum.enteties.Student;
import mladen.sobat.singidunum.enteties.Subject;

@Component("exsamsDao")
public class ExsamsDAO {
	private NamedParameterJdbcTemplate jdbc;
	
	public void setDataSource (DataSource ds) {
		this.jdbc =new NamedParameterJdbcTemplate(ds);
	}
	public List<Exsams> getExsams(){
		
	return jdbc.query("SELECT * FROM exsams ",new RowMapper<Exsams>() {

		@Override
		public Exsams mapRow(ResultSet rs, int rowNum) throws SQLException {
			Exsams exsams = new Exsams();
			exsams.setId(rs.getInt("id"));
			exsams.setDate(rs.getDate("date"));
			exsams.setProfesor((Profesor)rs.getObject("profesor"));
			exsams.setStudent((Student)rs.getObject("student"));
			exsams.setSubject((Subject)rs.getObject("subject"));
			
			return exsams;
		}
		});
	}
	public Exsams getExsamsById(int id) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.queryForObject("SELECT * FROM exsams where  id=:id",params,new RowMapper<Exsams>() {

			@Override
			public Exsams mapRow(ResultSet rs, int rowNum) throws SQLException {
				Exsams exsams =new Exsams();
				exsams.setId(rs.getInt("id"));
				exsams.setDate(rs.getDate("date"));
				exsams.setProfesor((Profesor)rs.getObject("profesor"));
				exsams.setStudent((Student)rs.getObject("student"));
				exsams.setSubject((Subject)rs.getObject("subject"));
				
				return exsams;
			}
		});
		
	}
	public boolean delete (int id) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id",id);
		return jdbc.update("DELETE FROM exsams where id=:id", params)==1;
	}
	public boolean create (Exsams exsams) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id",exsams.getId());
		params.addValue("date",exsams.getDate());
		params.addValue("profesor", exsams.getProfesor());
		params.addValue("student", exsams.getStudent());
		params.addValue("subject", exsams.getSubject());
		
		return jdbc.update("INSERT INTO exsams(id,date,profesor,student,subject)"
				+"VALUES (:id,:date,:profesor,:student ,:subject)", params)==1;
		
		
	}
	public boolean update(Exsams exsams ) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(exsams);
		return jdbc.update("UPDATE exsams SET date = :date,profesor=:profesor,student=:student,subject=:subject where id=:id", params)==1;
		
		
	}
}

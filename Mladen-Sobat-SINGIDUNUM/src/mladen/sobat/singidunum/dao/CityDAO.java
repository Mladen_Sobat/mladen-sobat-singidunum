package mladen.sobat.singidunum.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import mladen.sobat.singidunum.enteties.City;

@Component("cityDao")
public class CityDAO {
	private NamedParameterJdbcTemplate jdbc;
	
	public CityDAO() {
		System.out.println("City dao created!");
	}
	
	@Autowired
	public void setDataSource(DataSource ds) {
		this.jdbc = new NamedParameterJdbcTemplate(ds);
		
	}
	public List<City> getCityes(){
		return jdbc.query("SELECT * FROM city",new RowMapper<City>(){
		
			@Override
			public City mapRow(ResultSet rs, int rowNum) throws SQLException {
				City city = new City();
				
				city.setId(rs.getInt("city_id"));
				city.setName(rs.getString("name"));
				city.setCountry(rs.getString("country"));
				return city;
			}
		});
	}
	public City getCityById(int id) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.queryForObject("SELECT * FROM city where city_id=:id", params ,
				new RowMapper <City>() {

			public City mapRow(ResultSet rs, int rowNum) throws SQLException {
                 City city = new City();
				
				city.setId(rs.getInt("city_id"));
				city.setName(rs.getString("name"));
				city.setCountry(rs.getString("country"));
				System.out.println("Confirmed: " + city);
				return city;
			}
			
		});
	}
	public boolean delete (int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id",id);
		return jdbc.update("DELETE FROM city where city_id=:id",params)==1;
	}
	public boolean create (City city) {
		MapSqlParameterSource params =new MapSqlParameterSource();
		params.addValue("id",city.getId());
		params.addValue("name",city.getName());
		params.addValue("country",city.getCountry());
		
		return jdbc.update("INSERT INTO city(city_id,name,country)"
				+ "VALUES (:id,:name,:country)",params)==1;
		
	}
	public Boolean update (City city) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(city);
		return jdbc.update("UPDATE city SET name=:name,country=:country WHERE city_id=:id, ", params)==1;
	}
}

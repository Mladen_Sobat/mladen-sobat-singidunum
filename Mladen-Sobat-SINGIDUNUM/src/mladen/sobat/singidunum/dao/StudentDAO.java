package mladen.sobat.singidunum.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSourceExtensionsKt;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import mladen.sobat.singidunum.enteties.City;
import mladen.sobat.singidunum.enteties.Student;

@Component("studentDao")
public class StudentDAO {
	private NamedParameterJdbcTemplate jdbc;
	
	public StudentDAO() {
		System.out.println("Succesfully loaded student dao");
	}

	@Autowired
	public void setDataSource(DataSource ds) {
		this.jdbc = new NamedParameterJdbcTemplate(ds);
	}

	public List<Student> getStudenti() {
		System.out.println("StudentDAO");
		return jdbc.query("SELECT * FROM student", new RowMapper<Student>() {

			@Override
			public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
				Student student = new Student();

				student.setId(rs.getInt("id"));
				student.setName(rs.getString("name"));
				student.setLastName(rs.getString("lastname"));
				student.setEmail(rs.getString("email"));
				student.setAdress(rs.getString("adresa"));
				student.setCity((City) rs.getObject("city"));
				student.setPhone(rs.getString("phone"));
				student.setCurrentYearOfStudy(rs.getInt("currentYearOfStudy"));

				System.out.println(student);
				return student;
			}

		});
	}

	public Student getStudentById(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.queryForObject("SELECT * FROM student where id=:id", params, new RowMapper<Student>() {

			@Override
			public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
				Student student = new Student();
				student.setId(rs.getInt("id"));
				student.setName(rs.getString("name"));
				student.setLastName(rs.getString("lastname"));
				student.setEmail(rs.getString("email"));
				student.setAdress(rs.getString("adresa"));
				student.setCity((City) rs.getObject("city"));
				student.setPhone(rs.getString("phone"));
				student.setCurrentYearOfStudy(rs.getInt("currentYearOfStudy"));

				return student;
			}
		});
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("DELETE FROM student where id=:id", params) == 1;
	}

	public boolean create(Student student) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", student.getId());
		params.addValue("name", student.getName());
		params.addValue("lastname", student.getLastName());
		params.addValue("Email", student.getEmail());
		params.addValue("adresa", student.getAdress());
		params.addValue("city", student.getCity());
		params.addValue("phone", student.getPhone());
		params.addValue("curentYearOfStudy", student.getCurrentYearOfStudy());

		return jdbc.update("INSERT INTO student (id,name,lastname,Email,adresa,city,phone,currentYearOfStudy)"
				+ " VALUES (:id, :name,:lastname,:Email,:adresa,:city,:phone,:curentYearOfStudy)", params) == 1;

	}

	public boolean update(Student student) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(student);
		return jdbc.update(
				"UPDATE student SET name=:name,lastname=:lastname,adresa=:adresa,city=:city,phone=:phone,curentYearOfStudy=:curentYearOfStudy WHERE id=:id",
				params) == 1;

	}
}

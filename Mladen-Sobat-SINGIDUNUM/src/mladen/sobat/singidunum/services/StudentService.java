package mladen.sobat.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mladen.sobat.singidunum.dao.StudentDAO;
import mladen.sobat.singidunum.enteties.Student;

@Service("studentService")
public class StudentService {

	private StudentDAO studentDao;
	
	@Autowired
	public void setStudentDao(StudentDAO studentDao) {
		this.studentDao=studentDao;
	}
	public List<Student>getCurrentStudent(){
		System.out.println("Student service");
		return studentDao.getStudenti();
	}
	public boolean create (Student student) {
		return studentDao.create(student);
	}
	public boolean delete (int id) {
		return studentDao.delete(id);
	}
	public boolean update(Student student) {
		return studentDao.update(student);
	}
}

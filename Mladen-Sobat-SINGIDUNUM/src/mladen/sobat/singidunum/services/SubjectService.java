package mladen.sobat.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mladen.sobat.singidunum.dao.SubjectDAO;
import mladen.sobat.singidunum.enteties.Subject;

@Service
public class SubjectService {
	@Autowired
	SubjectDAO subjectDao;
	
	public List <Subject>getSubject(){
		return subjectDao.getSubjects();
	}
	public Subject getSubject(int id) {
		return subjectDao.getSubjectById(id);
		
	}
	public boolean delete (int id) {
		return subjectDao.delete(id);
		
	}
	public boolean create (Subject subject) {
		return subjectDao.create(subject);
	}
	public boolean update (Subject subject) {
		return subjectDao.update(subject);
	}
	public boolean  insertSubject(Subject subject) {
		return subjectDao.create(subject);
		
	}
}

package mladen.sobat.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mladen.sobat.singidunum.dao.ExsamsDAO;
import mladen.sobat.singidunum.enteties.Exsams;

@Service
public class ExamsService {
	 
	@Autowired
	ExsamsDAO exsamDao;
	
	public List<Exsams>getExsams(){
		return exsamDao.getExsams();
		
	}
	public Exsams getEbyId(int id) {
		return exsamDao.getExsamsById(id);
		
	}
	public boolean delete (int id) {
		return exsamDao.delete(id);
	}
	public boolean create(Exsams exsams) {
		return exsamDao.create(exsams);
		
	}
	public boolean update(Exsams exsams) {
		return exsamDao.update(exsams);
	}
	public void setExamsDao(ExsamsDAO exsamDao) {
		this.exsamDao=exsamDao;
	}
}

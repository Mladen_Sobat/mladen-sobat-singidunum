package mladen.sobat.singidunum.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mladen.sobat.singidunum.dao.CityDAO;
import mladen.sobat.singidunum.dao.TitleDAO;
import mladen.sobat.singidunum.enteties.City;
import mladen.sobat.singidunum.enteties.Student;
import mladen.sobat.singidunum.enteties.Title;
@Service
public class CityService {
@Autowired

	CityDAO cityDao;
 	 
   public List <City> getCityes(){
	   return cityDao.getCityes();
	   
   }
   public City getCity(int id) {
	   return cityDao.getCityById(id);
   }
   public boolean delete (int id) {
	   return cityDao.delete(id);
   }
   
 
	
   public boolean update (City city) {
	   return cityDao.update(city);
   }
   public void setCityDao(CityDAO cityDao) {
	     this.cityDao=cityDao;
}
public List<City> getAllCityes() {
	
	return cityDao.getCityes();
}
public boolean create( City city) {
	return cityDao.create(city);
	
}

}


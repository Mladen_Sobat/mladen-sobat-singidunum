package mladen.sobat.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mladen.sobat.singidunum.dao.TitleDAO;
import mladen.sobat.singidunum.enteties.Title;

@Service
public class TitleService {

	@Autowired
	
   TitleDAO titleDao;

public List<Title>getAllTitles(){
	return titleDao.getTitles();
}
	
	public Title getTitle(int id) {
		return titleDao.getTitleById(id);
	}
	public boolean deleteTitle (int id) {
		return titleDao.delete(id);
	}
	public boolean update (Title title) {
		return titleDao.update(title);
		
	}
	
	public void setTitleDao(TitleDAO titleDao) {
	     this.titleDao=titleDao;
	
    }
}

package mladen.sobat.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mladen.sobat.singidunum.dao.ProfesorDAO;
import mladen.sobat.singidunum.enteties.Profesor;

@Service("profesorService")
public class ProfesorService {
	
	private  ProfesorDAO profesorDao;
	
	@Autowired
	public  void setProfesorDao(ProfesorDAO profesorDao) {
		this.profesorDao=profesorDao;
	}
	public List<Profesor>getCurrentProfesor(){
		return profesorDao.getProfesori();
	}
	public boolean create (Profesor profesor) {
		return profesorDao.create(profesor);
	}
	public boolean delete (int id) {
		return profesorDao.delete(id);
	}

	public boolean update (Profesor profesor) {
		return profesorDao.update(profesor);
	}
	
	}


package mladen.sobat.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mladen.sobat.singidunum.enteties.City;
import mladen.sobat.singidunum.enteties.Student;
import mladen.sobat.singidunum.services.CityService;
import mladen.sobat.singidunum.services.StudentService;

@Controller
public class StudentController {
	
	private StudentService studentService;
	@Autowired
	private CityService cityService;
	
	@Autowired
	public void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}
	
	
	@RequestMapping("/student")
	public String showStudent(Model model) {
		System.out.println("Radi student");
		List<Student> student = studentService.getCurrentStudent();
		model.addAttribute("student",student);
		return "student";
	}
	@RequestMapping("/createNewStudent")
	public String createStudent(Model model) {
//		List<City> cities = cityService.getAllCityes();
//		model.addAttribute("cities",cities);
		return "createNewStudent";
	}
	@RequestMapping(value="/doCreateStudent", method=RequestMethod.POST)
	public String doCreate(Model model, @Valid Student student, City city, BindingResult result) {
		System.out.println(student);
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError> errors =result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		}
		else {
			System.out.println("Form validatio succesfully");
		}
//		City newCity = cityService.getCity(city.getId());
//		student.setCity(newCity);
		System.out.println(student);		
		studentService.create(student);
		model.addAttribute("student",student);
		return "studentCreated";
	}
	
	}


package mladen.sobat.singidunum.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mladen.sobat.singidunum.enteties.Profesor;
import mladen.sobat.singidunum.enteties.Subject;
import mladen.sobat.singidunum.services.ProfesorService;
import mladen.sobat.singidunum.services.SubjectService;

public class SubjectController {

	@Autowired
	private SubjectService subjectService;
	@Autowired
	private ProfesorService profesorService;
	
	@RequestMapping("/subjects")
	public String showSubjects(Model model) {
		List<Subject> subjects = subjectService.getSubject();
		model.addAttribute("subjects", subjects);
		return "subjects";			
	}
	
	@RequestMapping("/subjects/create")
	public String createSubject(Model model) {
		List<Profesor> professors = profesorService.getCurrentProfesor();
		model.addAttribute("professors", professors);
		return "createsubject";
	}
	
	@RequestMapping(value = "/doCreateSubject", method = RequestMethod.POST)
	public String doCreate(Model model, Subject subject, Profesor profesor, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid!");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		}
		else {
			System.out.println("Form validated successfully!");
		}
		return "createdSubject";
		
		
	
	}
	
}


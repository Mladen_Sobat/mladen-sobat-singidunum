package mladen.sobat.singidunum.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String showHome (){
		System.out.println("Radi");
		return "home";
	}
	@RequestMapping("/home/students")
	public String showStudentsHome() {
		return "studentshome";
	}
	
	@RequestMapping("/home/professors")
	public String showProfessorsHome() {
		return "professorshome";
	}
	
	@RequestMapping("/home/subjects")
	public String showSubjectsHome() {
		return "subjectshome";
	}
	
	@RequestMapping("/home/other")
	public String showOtherHome() {
		return "otherhome";
	}

}


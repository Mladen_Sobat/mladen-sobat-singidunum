package mladen.sobat.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mladen.sobat.singidunum.enteties.City;
import mladen.sobat.singidunum.services.CityService;

@Controller
public class CityController {
	 
	@Autowired
	private CityService cityService;
	
@RequestMapping("/city")
public String showCities (Model model) {
	List <City> cities = cityService.getAllCityes();
	System.out.println(cities);
	model.addAttribute("cities",cities);
	return "cities";
}
@RequestMapping("/createNewCity")

public String createCity(Model model) {
	return "createNewCity";
}
@RequestMapping(value = "/doCreateCity", method = RequestMethod.POST)
public String doCreate(Model model, @Valid City city, BindingResult result) {
	if (result.hasErrors()) {
		System.out.println("Form is not valid!");
		List<ObjectError> errors = result.getAllErrors();
		for (ObjectError e : errors) {
			System.out.println(e.getDefaultMessage());
		}
		return "error";
	} else {
		System.out.println("Form validated successfully!");
	}

	System.out.println(city);
	cityService.create(city);

	model.addAttribute("city", city);
	
	return "createdCity";
}
}
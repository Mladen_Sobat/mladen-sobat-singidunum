package mladen.sobat.singidunum.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mladen.sobat.singidunum.enteties.City;
import mladen.sobat.singidunum.enteties.Profesor;
import mladen.sobat.singidunum.enteties.Title;
import mladen.sobat.singidunum.services.CityService;
import mladen.sobat.singidunum.services.ProfesorService;
import mladen.sobat.singidunum.services.TitleService;

@Controller
public class ProfesorController {

		private ProfesorService profesorService;


@Autowired
private ProfesorService professorService;
@Autowired
private CityService cityService;
@Autowired
private TitleService titleService;
		public void setProfesorService (ProfesorService profesorService) {
			this.profesorService = profesorService;
			
		}
		@RequestMapping("/profesor")
		public String showProfesor(Model model) {
			List<Profesor> profesor =profesorService.getCurrentProfesor();
			model.addAttribute("profesor",profesor);
			return "profesor";
			
		}
		@RequestMapping("/createNewProfesor")
		public String createProfesor(Model model) {
			List<City> cities = cityService.getAllCityes();
			List<Title> titles = titleService.getAllTitles();
			System.out.println("Profsor create");
			System.out.println(cities);
			System.out.println(titles);
			model.addAttribute("cities", cities);
			model.addAttribute("titles", titles);
			return "createNewProfesor";
			
		}
		@RequestMapping(value="/doCreateProfesor", method=RequestMethod.POST)
	public String doCreate(Model model,Profesor profesor,City city,Title title,BindingResult result) {
			if(result.hasErrors()) {
				System.out.println("Form is not valid!");
				List<ObjectError> errors = result.getAllErrors();
				for(ObjectError e:errors) {
					System.out.println(e.getDefaultMessage());
				}
				return "error";
			}
				else {
					System.out.println("Form validated successfully");
				}
			City professorCity = cityService.getCity(city.getId());
			Title professorTitle = titleService.getTitle(title.getTitle_id());
			
			profesor.setCity(professorCity);
			profesor.setTitle(professorTitle);
			
			System.out.println(profesor);
			
			profesorService.create(profesor);
		
			model.addAttribute("profesor", profesor);
			
			return "profesorCreated";
		}
		
}
